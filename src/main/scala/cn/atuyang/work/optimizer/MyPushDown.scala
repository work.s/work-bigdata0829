package cn.atuyang.work.optimizer

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.catalyst.plans.logical.{LogicalPlan, Project}
import org.apache.spark.sql.catalyst.rules.Rule

case class MyPushDown(spark: SparkSession) extends Rule[LogicalPlan] {

  override def apply(plan: LogicalPlan): LogicalPlan = {
    logWarning("自定义规则触发")
    plan transform {
      case project@Project(projectList, _) =>
        logWarning("匹配到 Project")
        projectList.foreach {
          name =>
            logWarning("字段名称:" + name)
        }
        project
    }
  }
}
