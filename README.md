# work-bigdata0829

作业1截图：
![img.png](img.png)

作业2.1：

建表

create table yyy(name string, age int);

查询

select name || '1', age from (select name || '1' as name, age from yyy where age = 12) where name = 'yang1' and 1=1 except select name, age from yyy where age = 13;

规则

从PushDownPredicates（谓词下推）规则中可以看到调用了CombineFilters规则

![img_4.png](img_4.png)
![img_2.png](img_2.png)
![img_3.png](img_3.png)

作业2.2：

延续前表

查询

select name || '1', age, 1.0 x, 'aaa' y, now() as z from (select name || '1' as name, age from yyy where age = 12 except select name, age from yyy) where name = 'yang1' and 1=1 order by x, y, z;

![img_5.png](img_5.png)
![img_6.png](img_6.png)
![img_7.png](img_7.png)
![img_8.png](img_8.png)
![img_9.png](img_9.png)

作业3

![img_10.png](img_10.png)